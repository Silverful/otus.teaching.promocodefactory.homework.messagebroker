﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoCodeConsumer: IConsumer<IReceivePromoCodeMessage>
    {
        private readonly IPromoCodeService _promocodeService;

        public PromoCodeConsumer(IPromoCodeService promocodeService)
        {
            _promocodeService = promocodeService;
        }

        public async Task Consume(ConsumeContext<IReceivePromoCodeMessage> context)
        {
            await _promocodeService.GivePromoCodesToCustomerAsync(context.Message);
        }
    }
}
