﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Messages;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromoCodeService
    {
        Task<IEnumerable<PromoCode>> GetAllPromocodesAsync();

        Task<PromoCode> GivePromoCodesToCustomerAsync(IReceivePromoCodeMessage request);
    }
}
