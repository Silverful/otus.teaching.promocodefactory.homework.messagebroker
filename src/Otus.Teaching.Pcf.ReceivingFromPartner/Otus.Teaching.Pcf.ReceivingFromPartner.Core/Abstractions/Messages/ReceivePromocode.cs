﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Messages
{
    public class ReceivePromocode
    {
        public PromoCode PromoCode { get; set; }
        public Guid? PartnerManagerId { get; set; }
    }
}
