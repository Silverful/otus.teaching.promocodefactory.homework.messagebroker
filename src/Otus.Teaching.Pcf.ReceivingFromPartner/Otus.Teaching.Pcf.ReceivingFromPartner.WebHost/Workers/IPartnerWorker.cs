﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Messages;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Workers
{
    public interface IPartnerWorker
    {
        Task SendReceivePromocodeMessage(ReceivePromocode message);
    }
}
