﻿using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Messages;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Workers
{
    public class PartnerWorker : IPartnerWorker
    {
        readonly IBus _bus;

        public PartnerWorker(IBus bus)
        {
            _bus = bus;
        }

        public async Task SendReceivePromocodeMessage(ReceivePromocode message)
        {
            await _bus.Publish(message);
        }
    }
}
