﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    public class Preference
        : BaseEntity
    {
        [Display(Name = "First Name")]
        public string Name { get; set; }
    }
}
