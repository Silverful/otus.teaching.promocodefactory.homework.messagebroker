﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeeController
        : Controller
    {
        private readonly IService<Employee> _employeeService;
        private readonly IRepository<Employee> _repository;

        public EmployeeController(IService<Employee> employeeService, IRepository<Employee> repository)
        {
            _employeeService = employeeService;
            _repository = repository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeService.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <param name="id">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeService.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Id = employee.Id,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Обновить количество выданных промокодов
        /// </summary>
        /// <param name="id">Id сотрудника, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/appliedPromocodes")]
        public async Task<IActionResult> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeService.UpdateAppliedPromocodesAsync(id);

            if (employee == null)
                return NotFound();

            return Ok();
        }

        [HttpGet("/[controller]/[action]")]
        public async Task<ActionResult> Employees()
        {
            var employees = await _employeeService.GetAllAsync();
            return View("EmployeesView", employees);
        }

        [HttpPost("/[controller]/[action]")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateWithReload([FromForm] Employee employee)
        {
            await _repository.AddAsync(employee);

            return await Employees();
        }

        [HttpPost("/[controller]/[action]")]
        public async Task<ActionResult> DeleteWithReload([FromForm] Employee employee)
        {
            await _repository.DeleteAsync(employee);

            return await Employees();
        }

        [HttpPost("/[controller]/[action]")]
        public async Task<ActionResult> Update([FromForm] Employee employee)
        {
            await _repository.UpdateAsync(employee);

            return await Employees();
        }
    }
}