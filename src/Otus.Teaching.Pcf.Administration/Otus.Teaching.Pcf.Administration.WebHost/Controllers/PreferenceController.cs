﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Controllers
{
    public class PreferenceAddRequest
    {
        public string Name { get; set; }
    }

    [ApiController]
    [Route("[controller]")]
    public class PreferenceController : Controller
    {
        private readonly IRepository<Preference> _repository;

        public PreferenceController(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> Preferences()
        {
            var preferences = await _repository.GetAllAsync();
            return View("PreferencesView", preferences);
        }

        [HttpPost("[action]")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateWithReload([FromForm] PreferenceAddRequest preference)
        {
            var pref = new Preference() { Name = preference.Name };
            await _repository.AddAsync(pref);

            return await Preferences();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> DeleteWithReload([FromForm] Preference preference)
        {
            await _repository.DeleteAsync(preference);

            return await Preferences();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> Update([FromForm] Preference preference)
        {
            await _repository.UpdateAsync(preference);

            return await Preferences();
        }
    }
}
