﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PromoCodeController : Controller
    {
        private readonly IRepository<PromoCode> _repository;

        public PromoCodeController(IRepository<PromoCode> repository)
        {
            _repository = repository;
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> PromoCodes()
        {
            var promoCodes = await _repository.GetAllAsync();
            return View("PromoCodesView", promoCodes);
        }

        [HttpPost("[action]")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateWithReload([FromForm] PromoCode promoCode)
        {
            await _repository.AddAsync(promoCode);

            return await PromoCodes();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> DeleteWithReload([FromForm] PromoCode promoCode)
        {
            await _repository.DeleteAsync(promoCode);

            return await PromoCodes();
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> Update([FromForm] PromoCode promoCode)
        {
            await _repository.UpdateAsync(promoCode);

            return await PromoCodes();
        }
    }
}
