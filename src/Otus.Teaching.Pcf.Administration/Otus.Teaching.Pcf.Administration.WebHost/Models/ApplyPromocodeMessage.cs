﻿using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class ApplyPromocodeMessage
    {
        public Guid PromocodeMessage { get; set; }
    }
}
