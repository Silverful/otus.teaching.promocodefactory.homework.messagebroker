﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PromoCodeAdministrationConsumer : IConsumer<ApplyPromocodeMessage>
    {
        private IService<Employee> _employeeService;

        public PromoCodeAdministrationConsumer(IService<Employee> employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<ApplyPromocodeMessage> context)
        {
            if (context.Message?.PromocodeMessage != null)
            {
                await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PromocodeMessage);
            }
        }
    }
}
