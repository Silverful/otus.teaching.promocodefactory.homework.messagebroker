﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class EmployeeService : IService<Employee>
    {
        private IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeReposity)
        {
            _employeeRepository = employeeReposity;
        }

        public async Task<IEnumerable<Employee>> GetAllAsync() =>
            await _employeeRepository.GetAllAsync();

        public async Task<Employee> GetByIdAsync(Guid id) =>
            await _employeeRepository.GetByIdAsync(id);

        public async Task<Employee> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return null;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return employee;
        }
    }
}
